package pl.akademiakodu.lotto.controller;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Lotto {

        private int number;

    public Lotto(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public static  Set<Integer> getNumbers(){

       Random random = new Random();
       Set<Integer> zbior = new HashSet<>();
       for(int i = 0; i <6;i++) {
           int number = random.nextInt(49) + 1;
           zbior.add(number);
       }
       return zbior;
   }

    @Override
    public String toString() {
        return " "+ getNumber();
    }
}

