package pl.akademiakodu.lotto.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @ResponseBody
    @GetMapping("/lotto")//Żądanie GET /hello ma obsłużyć hello
    public String lotto(ModelMap modelMap){

        modelMap.put("zbior", Lotto.getNumbers());

        return "lotto";
    }
}
